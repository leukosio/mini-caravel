#!/usr/bin/env python3
import shlex
import subprocess

import yaml

config = subprocess.check_output(
    "docker-compose -f ./ops/docker-compose.yaml -f ./ops/lcl/docker-compose.yaml config".split()
)
config = yaml.safe_load(config)

PGVARS = "DP_STORE_PGDATABASE DP_STORE_PGHOST DP_STORE_PGPASSWORD DP_STORE_PGPORT DP_STORE_PGUSER".split()

for var in PGVARS:
    value = config["services"]["air-schd"]["environment"][var]
    print(f"export {var}={shlex.quote(value)}")
