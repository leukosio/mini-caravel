#!/usr/bin/env python3
import subprocess
import sys
from pathlib import Path

import click


def is_python(file):
    if not file.is_file():
        return False

    if file.suffix == ".py":
        return True

    first_line = file.open("rb").readline()
    if first_line.startswith("#!/usr/bin/env python".encode("ascii")):
        return True
    if first_line.startswith("#!/usr/bin/python".encode("ascii")):
        return True
    return False


def python_files():
    # NOTE can't use Path.glob as it includes . dirs, like .venv,
    # which we don't want. 20210502:mb
    root = Path(__file__).parent.parent

    def walk(p):
        if is_python(p):
            yield p
        elif p.is_dir():
            for child in p.iterdir():
                if not child.name.startswith("."):
                    yield from walk(child)

    return walk(root)


def do_check():
    check_ok = True
    for file in python_files():
        print(f"# {file}")
        proc = subprocess.run(["black", "--check", str(file)])
        check_ok = check_ok and proc.returncode == 0

        proc = subprocess.run(["isort", "-c", str(file)])
        check_ok = check_ok and proc.returncode == 0

        proc = subprocess.run(["flake8", str(file)])
        check_ok = check_ok and proc.returncode == 0

    sys.exit(0 if check_ok else 1)


def do_reformat():
    for file in python_files():
        print(f"# {file}")

        subprocess.run(["black", str(file)])
        subprocess.run(["isort", str(file)])


@click.command()
@click.option("-c", "--check", type=bool, default=False, is_flag=True)
def main(check):
    if check:
        do_check()
    else:
        do_reformat()
        do_check()


if __name__ == "__main__":
    main()
