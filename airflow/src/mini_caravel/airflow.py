from datetime import datetime
from urllib.parse import urlencode

import requests
from airflow.models import XCom
from airflow.operators.python_operator import PythonOperator
from airflow.utils.db import provide_session

from mini_caravel.config import CONFIG


@provide_session
def cleanup_xcom(context, session=None):
    session.query(XCom).filter(XCom.dag_id == context["ti"].dag_id).delete()


def send_failure_to_slack(context):
    if CONFIG.DISABLE_SLACK_ALERTS:
        return True

    ti = context["ti"]
    task_id = ti.task_id
    dag_id = ti.dag_id

    query = dict(
        task_id=task_id, dag_id=dag_id, execution_date=ti.execution_date.isoformat()
    )
    url = "https://airflow.mini-caravel.com/admin/airflow/log?" + urlencode(query)

    webhook_url = CONFIG.SLACK_WEBHOOK_URL

    markdown = f"! Task <{url}|{dag_id}.{task_id}> failed"
    payload = dict(
        type="mrkdwn",
        text=markdown,
    )

    requests.post(webhook_url, json=payload)


def make_python_operator(task_id, callable, cls=PythonOperator, **kwargs):
    if callable is None:
        raise ValueError("Missing required argument callable")
    if CONFIG.FAIL_FAST:
        kwargs["retries"] = 0
        kwargs.pop("retry_delay", None)
    return cls(
        task_id=task_id,
        provide_context=True,
        python_callable=lambda ti, **kwargs: callable(ti),
        **kwargs,
    )


# NOTE you might be tempeted to try to write this a generator function
# (so placing the DAG constructor call directly in
# here). don't. there's a lot of airflow magic going on associate DAGs
# with their files and, sadly, it all breaks down if the pipeline
# files don't import the DAG module and they the constructo's caller
# isn't in the same file. code is being too clever :( 20210427:mb
def make_dag_init_args(
    dag_id, description="", schedule_interval=None, alert_slack_on_failure=True
):
    return dict(
        dag_id=dag_id,
        description=description,
        default_args=dict(
            owner="mini-caravel",
            depends_on_past=False,
            email=["dwh@mini-caravel.com"],
            email_on_failure=False,
            email_on_retry=False,
            on_failure_callback=send_failure_to_slack
            if alert_slack_on_failure is True
            else None,
            retries=0,
        ),
        schedule_interval=schedule_interval if CONFIG.SCHEDULES_ENABLED else None,
        start_date=datetime(2020, 8, 1),
        max_active_runs=1,
        catchup=False,
        on_success_callback=cleanup_xcom,
    )
