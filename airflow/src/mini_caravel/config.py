import os
from pathlib import Path


def _from_env(key, default, required, parser):
    v = os.environ.get(key)
    if v is None:
        if required:
            raise ValueError(f"Missing required config variable {key}")
        else:
            return default
    else:
        return parser(v)


def as_dirpath(key, default=None, required=False):
    return _from_env(key, default, required, parser=lambda str: Path(str.rstrip("/")))


def as_str(key, default=None, required=False):
    return _from_env(key, None, required, parser=lambda str: str)


def as_bool(key, default=False, required=False):
    return _from_env(
        key, default, required, parser=lambda str: str.lower() in ["yes", "1", "true"]
    )


class Config:
    def __init__(self):
        self.DP_ROOT = as_dirpath("DP_ROOT")
        self.SCHEDULES_ENABLED = as_bool("DP_SCHEDULES_ENABLED")
        self.DISABLE_SLACK_ALERTS = as_bool("DP_DISABLE_SLACK_ALERTS")
        self.SLACK_WEBHOOK_URL = as_bool("DP_SLACK_WEBHOOK_URL")
        if not self.DISABLE_SLACK_ALERTS and not self.SLACK_WEBHOOK_URL:
            raise ValueError(
                "slack alters are enabled but SLACK_WEBHOOK_URL is not set."
            )
        self.FAIL_FAST = as_bool("DP_FAIL_FAST")

        self.DBT_IMAGE = as_str("DP_DBT_IMAGE")
        if self.DBT_IMAGE is None:
            raise ValueError("Missing required config variables DBT_IMAGE")

        self.AWS_ACCESS_KEY_ID = as_str("AWS_ACCESS_KEY_ID")
        self.AWS_SECRET_ACCESS_KEY = as_str("AWS_SECRET_ACCESS_KEY")

        self.STORE_PGHOST = as_str("DP_STORE_PGHOST", required=True)
        self.STORE_PGPORT = as_str("DP_STORE_PGPORT", default=5432)
        self.STORE_PGDATABASE = as_str("DP_STORE_PGDATABASE", default="postgres")
        self.STORE_PGUSER = as_str("DP_STORE_PGUSER", default="postgres")
        self.STORE_PGPASSWORD = as_str("DP_STORE_PGPASSWORD", required=True)


CONFIG = Config()
