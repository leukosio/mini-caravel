import csv
import io
import tempfile
from datetime import datetime
from pathlib import Path
from pprint import pprint  # noqa: F401

import boto3
import psycopg2
from airflow import DAG
from airflow.operators.bash import BashOperator

from mini_caravel.airflow import make_dag_init_args, make_python_operator
from mini_caravel.config import CONFIG


class Bucket:
    def __init__(self):
        self.session = boto3.Session(
            aws_access_key_id=CONFIG.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=CONFIG.AWS_SECRET_ACCESS_KEY,
        )
        self.s3 = self.session.resource("s3")
        self.bucket = self.s3.Bucket("csv-upload")
        self.objects = self.bucket.objects

    def object_contents(self, key, encoding="utf-8"):
        stream = io.BytesIO()
        self.bucket.Object(key).download_fileobj(stream)
        value = stream.getvalue()
        if encoding is not None:
            value = value.decode(encoding)
        return value

    def download_object(self, key, file):
        if isinstance(file, Path):
            file = file.open("wb")
        self.bucket.Object(key).download_fileobj(file)


def extract_csvs_to_s3(ti):
    bucket = Bucket()

    latest_prefix = bucket.object_contents("csv-upload")
    print(f"Will grab latest batch from {latest_prefix}")

    dir = tempfile.mkdtemp()

    for o in bucket.objects.filter(Prefix=latest_prefix):
        key = o.key
        name = key.split("/")[-1]
        file = Path(dir) / name
        bucket.download_object(key, file)

    return dir


def pgconnect():
    return psycopg2.connect(
        dbname=CONFIG.STORE_PGDATABASE,
        host=CONFIG.STORE_PGHOST,
        port=CONFIG.STORE_PGPORT,
        user=CONFIG.STORE_PGUSER,
        password=CONFIG.STORE_PGPASSWORD,
    )


def load_to_postgres(filename):
    def task(ti):
        tmp_dir = ti.xcom_pull(task_ids=["extract"])[0]
        table_name = filename.lower() + "_raw"
        print(f"Will load {tmp_dir}/{filename}.csv to extract.{table_name}")
        conn = pgconnect()
        cur = conn.cursor()
        cur.execute("CREATE SCHEMA IF NOT EXISTS extract;")
        cur.execute(
            f"CREATE TABLE IF NOT EXISTS extract.{table_name} (extracted_at timestamptz, data jsonb)"
        )
        conn.commit()

        now = datetime.utcnow().isoformat() + "+00:00"
        path = Path(tmp_dir) / (filename + ".csv")
        with path.open("r", newline="", encoding="utf-16") as file:
            reader = csv.reader(
                file,
                delimiter=";",
                quotechar='"',
                doublequote=True,
                strict=True,
                skipinitialspace=True,
            )
            fieldnames = next(reader)

            def quote(str):
                return psycopg2.extensions.QuotedString(str).getquoted().decode("utf-8")

            quoted_field_names = [quote(field) for field in fieldnames]
            build_object_args = [field + ", %s::text" for field in quoted_field_names]
            build_object_arg_list = ", ".join(build_object_args)
            build_object = f"jsonb_build_object({ build_object_arg_list })"
            stmt = f"INSERT INTO extract.{table_name} (extracted_at, data) VALUES (%s::timestamptz, {build_object})"
            print(f"Inserting data with `{stmt}`")
            cur.execute(f"TRUNCATE TABLE extract.{table_name};")
            psycopg2.extras.execute_batch(cur, stmt, ([now] + row for row in reader))
            conn.commit()

    return task


def cleanup_temp_dir(ti):
    tmp_dir = ti.xcom_pull(task_ids=["extract"])[0]
    print(f"Will delete {tmp_dir}")
    # shutil.rmtree(tmp_dir)


def make_dbt_operator(task_id, command):
    bash_command = f"""
        docker run --rm
            -e DP_STORE_PGHOST
            -e DP_STORE_PGPORT
            -e DP_STORE_PGDATABASE
            -e DP_STORE_PGUSER
            -e DP_STORE_PGPASSWORD
            '{CONFIG.DBT_IMAGE}'
            dbt --no-use-colors {command}
    """
    bash_command = bash_command.replace("\n", "")
    return BashOperator(
        task_id=task_id,
        bash_command=bash_command,
        env=dict(
            DP_STORE_PGHOST="172.17.0.1",
            DP_STORE_PGPORT=CONFIG.STORE_PGPORT,
            DP_STORE_PGDATABASE=CONFIG.STORE_PGDATABASE,
            DP_STORE_PGUSER=CONFIG.STORE_PGUSER,
            DP_STORE_PGPASSWORD=CONFIG.STORE_PGPASSWORD,
        ),
    )


def make_dag():
    dag = DAG(
        **make_dag_init_args(
            "extract-csv",
            description="Load CSV data.",
            schedule_interval="@daily",
        )
    )
    with dag:
        download = make_python_operator("extract", extract_csvs_to_s3)
        dbt_run = make_dbt_operator("dbt.run", "run -m source:external_csv+")

        files = (
            "hello-data-world".split()
        )
        for filename in files:
            (
                download
                >> make_python_operator(f"load.{filename}", load_to_postgres(filename))
                >> dbt_run
            )

        (
            dbt_run
            >> make_dbt_operator("dbt.test", "test")
            >> make_python_operator("cleanup", cleanup_temp_dir)
        )

    return dag


dag = make_dag()
