# Mini-Caravel - A basic data platform setup

[[_TOC_]]

## Quick Start

To run an instance locally:

```sh
./ops/lcl/up
```


NOTE: You will need to set `AWS_ACCESS_KEY_ID` and
`AWS_SECRET_ACCESS_KEY` before running.

This will use docker-compose to setup 3 containers (2 airflow and one
db) running a full local installation of the data platform. The
`ops/lcl/run-dbt` script is designed to work with this setup.

- Visit `http://loalhost:8088` to navigate airflow's web interface
- postgres store is available via 127.0.0.1:5432 (airflow's database,
  for task state and user management, etc., is not by default exposed)
- passwords for lcl services are in `./ops/lcl/env.sh`

## Architecture and Flow

- Data is loaded from the outside world and stored, as unprocessed
  json blobs, in `_raw` tables in the `extract` schema.
- dbt is use to transform these raw tables into business data
- Airflow cordinates jobs and tasks

### Moving Parts

The are 4 main moving to the data platform:

1. airflow - 3 containers (services in docker compose's terminology). the webserver, the scheduler/worker, and the db (which is not the data platform store, just airflow's internal state)
2. dbt - This isn't really a service, we have the dbt code at `./dbt` and run dbt against the store when triggered by airflow.
3. postgres store - This is just a plain postgres db.
4. metabase - While this isn't part of the "data as a product" production pipeline it is an essential tool used for development both locally and on staging.

### Import DAGs

- `extract-csv` - example DAG which grabs a CSV from s3 and loads as a _raw table in postgres.

## Source Code Layout

- `airflow/src/mini_caravel/` the actual pyton code for loading data
- `airflow/src/mini_caravel/dags` the airflow dag definitions
- `dbt/models` the models used to transform the raw data loaded by airflow
- `dbt/models/schema.yml` defines the sources for dbt (so `{{
  source(...) }}` can be used to defined model dependencies) and tests
- `ops` all the code and scripts required to build, run, and deploy the data platform
- `store` exists for consistency, but only defined the docker image
  used for the data ware house store (which itself is just a plain
  postrgres:13 image)

## Local Development

### Running DBT

The easiest way to run dbt, assuming you have airflow and the store
running in the local docker setup, is via a local docker container:

`./ops/lcl/run-dbt <dbt args go here>`

verify your setup and credentials with:

```sh
$ ./ops/lcl/run-dbt debug
```

If you're running on windows it might be easier to use venv instead:

1. Create a venv.
2. Set the following environment variables:
   - `DBT_PROFILES`
   - `DP_STORE_PGPASSWORD`
   
   The following env vars have defaults asusming you're running
   against a local docker-ified store:
   - `DP_STORE_PGHOST=127.0.0.1`
   - `DP_STORE_PGUSER=pstgres`
   - `DP_STORE_PGPORT=4321`
   - `DP_STORE_PGDATABASE=postgres`
3. `cd dbt && pip install -r requirements.txt`
4. `cd dbt && dbt debug`

### Useful Tools

- For setting the various environment variables (there are quite a few
   of them once you into modifying the data platform)
   [direnv](https://direnv.net/) along with a `.envrc` file is very
   convenient.
- [pyenv](https://github.com/pyenv/pyenv) is great for managing
  multiple venvs. Since DBT and airflow have conflicting requirements
  it can be very convenient to have different virtual envs just by
  changing directories.

## STG, PRD, and Deployment Pipeline 

### CI

We use gitlab's cicd tooling to build images and update the staging
and production environments.

The ci job runs in a custom container which is built by `./ops/cicd/Dockerfile`

See
https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-by-using-gitlab-cicd
for details on how to configure this. Unless you are editing the image
itself you shouldn't need to do this though.

stg and prd are both labelled as gtilab environments, you can see what's deployed to each here:

https://gitlab.com/leukosio/mini-caravel/-/environments

### STG

Deployment to staging is done by pushing commits to the branch named `stg`.

Stagging is configured in 2 places:

1. the common simple settings are defined in
   `./ops/docker-compose.yaml` and `./ops/stg/docker-compose.yaml`
   (they are merged together by docker-compose when deploying).
2. secrets are set in gitlab in the ENV var (there's one ENV for stg
   and one ENV for prd environments)
   
STG's airflow listens on the host's port 9090.

### PRD

Deployment to production is done by pushing commits to the branch
named `master` (could be renamed `prd` for consistency).

Like staging, production is configured in 2 places:

1. the common simple settings are defined in
   `./ops/docker-compose.yaml` and `./ops/prd/docker-compose.yaml`
   (they are merged together by docker-compose when deploying).
2. secrets are set in gitlab in the ENV var.

PRD's airflow listens on the host's port 8080.

## Tests

No tests yet.

Use `./ops/do-reformat` to lint and run static checks (flake8) on code
before pushing.

## Infra
