select
  timestamp (data->>'date') with time zone as date
  ,data->>'name' as name
  ,data->>'value' as value
from {{ source('external_csv', 'hello_data_world') }}
